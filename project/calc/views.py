from django.shortcuts import render
from django.http import HttpResponse

def sumar(request,num1,num2):
	resultado = num1 + num2;
	return HttpResponse(resultado)
	
def restar(request,num1,num2):
	resultado = num1 - num2;
	return HttpResponse(resultado)
	
	
def multiplicar(request,num1,num2):
	resultado = num1 * num2;
	return HttpResponse(resultado)
	
def dividir(request,num1,num2):
	try:
		resultado = num1 / num2;
		return HttpResponse(resultado)
		
	except ZeroDivisionError:
		return HttpResponse("¡Error! No se puede dividir entre cero.")



