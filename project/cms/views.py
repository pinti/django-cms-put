from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt

formulario = """
No existe valor en la base de datos para esa llave
<p>Introducela
<p>
<form action = "" method = "POST">
	valor :<input type = "text" name = "valor">
	<br/> <input type = "submit" value = "Enviar">
	
</form>
"""

@csrf_exempt
def get_content(request,llave):
	
	if request.method == "POST":
		valor = request.POST['valor']
		c= Contenido(clave=llave, valor=valor)
		c.save()
		
	if request.method == "PUT":
		try:
			contenido = Contenido.objects.get(clave=llave)
			valor = request.body.decode("utf-8")
			contenido.valor = valor
			contenido.save()
		except Contenido.DoesNotExist:
			c= Contenido(clave=llave, valor=valor)
			c.save()
		
	try:
		contenido = Contenido.objects.get(clave=llave)
		respuesta = contenido.valor
	except Contenido.DoesNotExist:
		respuesta = formulario
	return HttpResponse(respuesta)


	

